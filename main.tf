terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.48.0"
    }
  }
}

provider "aws" {
  region  = var.region
  profile = "itech"
}

data "aws_eks_cluster_auth" "default" {
  name = module.eks.cluster_name
}

provider "kubernetes" {
  host                   = module.eks.cluster_endpoint
  cluster_ca_certificate = base64decode(module.eks.cluster_ca_certificate)
  token                  = data.aws_eks_cluster_auth.default.token
}

provider "helm" {
  kubernetes {
    host                   = module.eks.cluster_endpoint
    cluster_ca_certificate = base64decode(module.eks.cluster_ca_certificate)
    token                  = data.aws_eks_cluster_auth.default.token
  }
}

module "eks" {
  source = "./modules/eks"

  # EKS Cluster Settings
  cluster_name                    = "eks-cluster"
  cluster_version                 = "1.24"
  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = true

  # VPC settings
  availability_zones_count = 2

  # Node group settings
  nodes_desired_size = 2
  nodes_max_size     = 5
  nodes_min_size     = 2

  # RDS Settings
  db_name           = "mydb"
  db_instance_class = "db.t3.micro"
  db_engine         = "postgres"
  db_username       = "postgres"
  db_password       = "password"
}

