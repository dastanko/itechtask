### 1. Design high available and fault tolerance infra on AWS for small application ( Micro service application ) please offer all the necessary services for micro service application ( ECS - EKS service at least )
![Semantic description of image](diagram.png)
```
   - Architecture:
       - VPC
           - 3 tier VPC networking with 2 subnets with in different AZs for high-availability
               - 1 - public - will host load balancers managed by EKS
               - 2 - app layer - will host EKS managed ec2 nodes through ASG
               - 3 - db layer - will host RDS instances
       - Route53
           - Weighted Routing
               - with 2 records
                   - main (will receive 100% of the traffic during normal)
                   - backup (will route the traffic to another region in case of emergency)
       - RDS
           - RDS instance in Multi-AZ mode, with one standBy instance in other region ready to take the load in case of fail of the primary
       - EKS cluster
           - will be hosted in 2 AZs for high availability
           - Cluster Autoscaler ready to take unexpected load
       - Application Helm Chart
           - Deployment resource - would include for undisrupted rolling update 
           - HorizontalPodAutoscaler ready to scale up and down based on the load
```

### 2. Provide Terraform module for creating your suggestion infrastructure on AWS ( Terraform module and Terraform code, 2 separate repository )

```
   - Check this repo.
```

### 3. create repository for sample application on GitHub or GitLab and design branch strategy for DEV - QA - Release

```
   - Branch strategy
       - main
           - will reflect the state of production environment
       - any development can be done in any branch
       - any change to main goes through approval process through a PR
   - Example:
       - Check in https://gitlab.com/dastanko/awesomepython/-/blob/main/.gitlab-ci.yml
   - Personal note:
       - I would prefer going from simple to complex, so I would choose trunk-based development,
        I think this approach fits to projects any size and complexity.
```

### 4. Provide CI process for sample application ( Include ChatOps process as well )

    - CI process
         - will be triggered on any push to any branch
         - will run unit tests
         - will build docker image
         - will push docker image to ECR
    - Example:
        - Check in https://gitlab.com/dastanko/awesomepython/-/blob/main/.gitlab-ci.yml

### 5. Provide CD process for deploying application to cluster ( Suggestion Argocd & Helm chart for deployment )

    - CD process
         - deployment will be triggered on any push to main branch
         - any commit to non-main branch will be deployed to development environment
         - any PR creation will be deployed to staging environment
    - Note:
        - I would use ArgoCD, but I don't have experience with it, so I used just Helm.
        - Additionaly I would set up a webhook to notify about deployment status.
        - I would add review environment for each PR or any non-main branch if I had more time.
    - Example:
        - Check in https://gitlab.com/dastanko/awesomepython/-/blob/main/.gitlab-ci.yml

### 6. Design Disaster recovery plan to migrating from one region to another ( suggestion - Data transfer from RDS - S3 ... )
```
    - Disaster recovery plan
         - Prerequisites:
            - Terraform setup would require change:
              - It should include restore of the RDS instance from database backup replicated to backup region.
         - Then my Disaster Recovery would consist of the following steps
            - Run my terraform setup which would restore RDS from a backup in a backup region.
            - Deploy the application to cluster in new region
            - Change Route53 record to point to new application ingress
```