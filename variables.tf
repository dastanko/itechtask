variable "cluster_name" {
  default = "eks-cluster"
}

variable "region" {
  default = "eu-central-1"
}

variable "backup_region" {
  default = "eu-central-2"
}

variable "profile" {
  default = "itech"
}