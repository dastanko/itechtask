resource "aws_eks_cluster" "this" {
  name     = var.cluster_name
  role_arn = aws_iam_role.cluster.arn
  version  = var.cluster_version

  vpc_config {
    #    security_group_ids      = [aws_security_group.eks_cluster.id]
    subnet_ids              = flatten([aws_subnet.app_subnets[*].id, aws_subnet.public_subnets[*].id])
    endpoint_private_access = var.cluster_endpoint_private_access
    endpoint_public_access  = var.cluster_endpoint_public_access
  }

  depends_on = [
    aws_iam_role_policy_attachment.cluster_AmazonEKSClusterPolicy
  ]
}

data "tls_certificate" "this" {
  url = aws_eks_cluster.this.identity[0].oidc[0].issuer
}

resource "aws_iam_openid_connect_provider" "this" {
  client_id_list  = ["sts.amazonaws.com"]
  thumbprint_list = [data.tls_certificate.this.certificates[0].sha1_fingerprint]
  url             = aws_eks_cluster.this.identity[0].oidc[0].issuer
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    condition {
      test     = "StringEquals"
      variable = "${replace(aws_iam_openid_connect_provider.this.url, "https://", "")}:sub"
      values   = ["system:serviceaccount:kube-system:aws-node"]
    }

    principals {
      identifiers = [aws_iam_openid_connect_provider.this.arn]
      type        = "Federated"
    }
  }
}

resource "aws_iam_role" "eks_vpc_cni_role" {
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json
  name               = "eks-vpc-cni-role"
}

resource "aws_iam_role_policy_attachment" "AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.eks_vpc_cni_role.name
}

resource "aws_eks_addon" "coredns" {
  cluster_name = aws_eks_cluster.this.name
  addon_name   = "coredns"
}

resource "aws_eks_addon" "kube_proxy" {
  cluster_name = aws_eks_cluster.this.name
  addon_name   = "kube-proxy"
}

resource "aws_eks_addon" "vpc_cni" {
  cluster_name             = aws_eks_cluster.this.name
  addon_name               = "vpc-cni"
  service_account_role_arn = aws_iam_role.eks_vpc_cni_role.arn
}

# EKS Cluster IAM Role
resource "aws_iam_role" "cluster" {
  name = "eks-cluster-role"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "cluster_AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.cluster.name
}

data "aws_security_group" "eks_sg" {
  tags = {
    "kubernetes.io/cluster/${aws_eks_cluster.this.name}" = "owned"
    "aws:eks:cluster-name"                               = aws_eks_cluster.this.name
  }

  depends_on = [aws_eks_cluster.this]
}

resource "aws_security_group_rule" "eks_sg_http_rule" {
  security_group_id = data.aws_security_group.eks_sg.id
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "eks_sg_https_rule" {
  security_group_id = data.aws_security_group.eks_sg.id
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_iam_policy" "aws_lb_controller_policy" {
  name = "aws_lb_controller_policy"
  policy = file("${path.module}/aws_lb_controller_policy.json")
}

resource "kubernetes_service_account" "aws_lb_controller" {
  metadata {
    name      = "aws-lb-controller"
    namespace = "kube-system"
  }

  depends_on = [aws_eks_cluster.this]
}

data "aws_iam_policy_document" "assume_lb_controller_role_policy" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    condition {
      test     = "StringEquals"
      variable = "${replace(aws_iam_openid_connect_provider.this.url, "https://", "")}:sub"
      values   = ["system:serviceaccount:kube-system:${kubernetes_service_account.aws_lb_controller.metadata.0.name}"]
    }

    principals {
      identifiers = [aws_iam_openid_connect_provider.this.arn]
      type        = "Federated"
    }
  }
}

resource "aws_iam_role" "aws_lb_controller_iam_role" {
  name               = "aws-lb-controller-iam-role"
  assume_role_policy = data.aws_iam_policy_document.assume_lb_controller_role_policy.json
}

resource "aws_iam_role_policy_attachment" "aws_lb_controller_policy_attachment" {
  policy_arn = aws_iam_policy.aws_lb_controller_policy.arn
  role       = aws_iam_role.aws_lb_controller_iam_role.name

  provisioner "local-exec" {
    command = "aws --profile ${var.profile} eks update-kubeconfig --name ${aws_eks_cluster.this.name} --region ${var.region}"
  }

  provisioner "local-exec" {
    command = "kubectl annotate serviceaccount -n kube-system ${kubernetes_service_account.aws_lb_controller.metadata.0.name} eks.amazonaws.com/role-arn=${aws_iam_role.aws_lb_controller_iam_role.arn} --overwrite"
  }
}

resource "helm_release" "aws_lb_controller" {
  chart      = "aws-load-balancer-controller"
  name       = "aws-load-balancer-controller"
  repository = "https://aws.github.io/eks-charts"
  namespace  = kubernetes_service_account.aws_lb_controller.metadata[0].namespace

  set {
    name  = "clusterName"
    value = aws_eks_cluster.this.name
  }

  set {
    name  = "serviceAccount.create"
    value = "false"
  }

  set {
    name  = "serviceAccount.name"
    value = kubernetes_service_account.aws_lb_controller.metadata[0].name
  }

  depends_on = [aws_iam_role_policy_attachment.aws_lb_controller_policy_attachment]
}

