# Global Settings
variable "region" {
  default = "eu-central-1"
}

variable "profile" {
  default = "itech"
}

# EKS cluster
variable "cluster_name" {
  default = "eks-cluster"
}

variable "cluster_version" {
  default = "1.24"
}

variable "cluster_endpoint_private_access" {
  default = true
}

variable "cluster_endpoint_public_access" {
  default = true
}

# EKS VPC configuration
variable "availability_zones_count" {
  default = 2
}

variable "subnet_size" {
  default = 4
}

variable "vpc_cidr_block" {
  default = "10.0.0.0/16"
}

# EKS Node Groups
variable "nodes_desired_size" {
  default = 2
}
variable "nodes_max_size" {
  default = 5
}
variable "nodes_min_size" {
  default = 3
}

variable "nodes_instance_type" {
  default = "t3.micro"
}

variable "key_path" {
  default = "~/.ssh/id_rsa.pub"
}

# RDS Settings
variable "db_name" {
  default = "mydb"
}

variable "db_instance_class" {
  default = "db.t3.micro"
}

variable "db_engine" {
  default = "postgres"
}

variable "db_username" {
  default = "master"
}
variable "db_password" {
  default = "foobarbaz"
}