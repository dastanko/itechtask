data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_vpc" "this" {
  cidr_block = var.vpc_cidr_block

  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "main-vpc"
  }
}

resource "aws_subnet" "public_subnets" {
  count                               = var.availability_zones_count
  vpc_id                              = aws_vpc.this.id
  cidr_block                          = cidrsubnet(aws_vpc.this.cidr_block, var.subnet_size, count.index)
  availability_zone                   = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch             = true
  private_dns_hostname_type_on_launch = "ip-name"



  tags = {
    Name                                = "Public Subnet ${count.index + 1}"
    "kubernetes.io/cluster/eks-cluster" = "shared"
    "kubernetes.io/role/elb"            = 1
  }
}

resource "aws_subnet" "app_subnets" {
  count                               = var.availability_zones_count
  vpc_id                              = aws_vpc.this.id
  cidr_block                          = cidrsubnet(aws_vpc.this.cidr_block, var.subnet_size, var.availability_zones_count + count.index)
  availability_zone                   = data.aws_availability_zones.available.names[count.index]
  private_dns_hostname_type_on_launch = "ip-name"

  tags = {
    Name                                = "App Subnet ${count.index + 1}"
    "kubernetes.io/cluster/eks-cluster" = "shared"
    "kubernetes.io/role/internal-elb"   = 1
  }
}

resource "aws_subnet" "db_subnets" {
  count             = var.availability_zones_count
  vpc_id            = aws_vpc.this.id
  cidr_block        = cidrsubnet(aws_vpc.this.cidr_block, var.subnet_size, 2*var.availability_zones_count + count.index)
  availability_zone = data.aws_availability_zones.available.names[count.index]

  tags = {
    Name = "DB Subnet ${count.index + 1}"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.this.id

  tags = {
    Name = "VPC IG"
  }

  depends_on = [aws_vpc.this]
}

resource "aws_route_table" "internet_rt" {
  vpc_id = aws_vpc.this.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "Internet Route Table"
  }
}

resource "aws_route_table_association" "public_subnet_asso" {
  count          = var.availability_zones_count
  subnet_id      = aws_subnet.public_subnets.*.id[count.index]
  route_table_id = aws_route_table.internet_rt.id
}

resource "aws_eip" "nat_eips" {
  count = var.availability_zones_count
  vpc   = true

  tags = {
    Name = "NAT EIP ${count.index + 1}"
  }
}

resource "aws_nat_gateway" "app_nat_gws" {
  count         = var.availability_zones_count
  subnet_id     = aws_subnet.public_subnets[count.index].id
  allocation_id = aws_eip.nat_eips[count.index].id

  tags = {
    Name = "App NAT Gateway ${count.index + 1}"
  }

  depends_on = [aws_internet_gateway.gw]
}

resource "aws_route_table" "app_rts" {
  count  = var.availability_zones_count
  vpc_id = aws_vpc.this.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.app_nat_gws.*.id[count.index]
  }

  tags = {
    Name = "App Route Table ${count.index + 1}"
  }
}

resource "aws_route_table_association" "app_subnet_asss" {
  count          = var.availability_zones_count
  route_table_id = aws_route_table.app_rts[count.index].id
  subnet_id      = aws_subnet.app_subnets[count.index].id
}
