resource "aws_db_subnet_group" "this" {
  name       = "my-db-subnet-group"
  subnet_ids = aws_subnet.db_subnets.*.id

  tags = {
    "Name" = "my-db-subnet-group"
  }
}

resource "aws_security_group" "db_sg" {
  name        = "db_sg"
  description = "Allow inbound traffic"
  vpc_id      = aws_vpc.this.id

  ingress {
    description     = "Postgres"
    from_port       = 5432
    to_port         = 5432
    protocol        = "tcp"
    security_groups = [data.aws_security_group.eks_sg.id]
  }
}

resource "aws_db_instance" "this" {
  db_name              = var.db_name
  instance_class       = var.db_instance_class
  allocated_storage    = 20
  db_subnet_group_name = aws_db_subnet_group.this.name
  engine               = var.db_engine
  multi_az             = true
  skip_final_snapshot  = true
  username             = var.db_username
  password             = var.db_password


  vpc_security_group_ids = [
    aws_security_group.db_sg.id
  ]
}